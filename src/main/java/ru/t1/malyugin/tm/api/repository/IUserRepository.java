package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    User create(String login, String password);

    User create(String login, String passwordHash, String email, Role role);

}