package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

}