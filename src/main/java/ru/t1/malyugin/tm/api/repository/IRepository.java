package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M extends AbstractModel> {

        Optional<M> findOneById(String id);

        Optional<M> findOneByIndex(Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {

            @Override
            public Optional<M> findOneById(String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            public Optional<M> findOneByIndex(Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }

        };
    }

    void clear();

    int getSize();

    M add(M model);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(List<M> models);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

}