package ru.t1.malyugin.tm.model;

import ru.t1.malyugin.tm.enumerated.Status;

public final class Project extends AbstractWBSModel {

    private String description = "";

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += String.format("%s: %s IN STATUS '%s'", id, name, status.getDisplayName());
        result += (isDescription ? " -> " + description : "");
        return result;
    }

}