package ru.t1.malyugin.tm.component;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.command.project.*;
import ru.t1.malyugin.tm.command.system.*;
import ru.t1.malyugin.tm.command.task.*;
import ru.t1.malyugin.tm.command.user.*;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.malyugin.tm.exception.system.CommandNotSupportedException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.CommandRepository;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;
import ru.t1.malyugin.tm.repository.UserRepository;
import ru.t1.malyugin.tm.service.*;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registryCommand(new ApplicationHelpCommand());
        registryCommand(new ApplicationAboutCommand());
        registryCommand(new ApplicationExitCommand());
        registryCommand(new ApplicationVersionCommand());
        registryCommand(new SystemInfoCommand());

        registryCommand(new ProjectChangeStatusByIdCommand());
        registryCommand(new ProjectChangeStatusByIndexCommand());
        registryCommand(new ProjectClearCommand());
        registryCommand(new ProjectCompleteByIdCommand());
        registryCommand(new ProjectCompleteByIndexCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectListShowCommand());
        registryCommand(new ProjectRemoveByIdCommand());
        registryCommand(new ProjectRemoveByIndexCommand());
        registryCommand(new ProjectShowByIdCommand());
        registryCommand(new ProjectShowByIndexCommand());
        registryCommand(new ProjectStartByIdCommand());
        registryCommand(new ProjectStartByIndexCommand());
        registryCommand(new ProjectUpdateByIdCommand());
        registryCommand(new ProjectUpdateByIndexCommand());

        registryCommand(new TaskBindToProjectCommand());
        registryCommand(new TaskUnbindFromProjectCommand());
        registryCommand(new TaskChangeStatusByIdCommand());
        registryCommand(new TaskChangeStatusByIndexCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new TaskCompleteByIdCommand());
        registryCommand(new TaskCompleteByIndexCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskListShowByProjectIdCommand());
        registryCommand(new TaskListShowCommand());
        registryCommand(new TaskRemoveByIdCommand());
        registryCommand(new TaskRemoveByIndexCommand());
        registryCommand(new TaskShowByIdCommand());
        registryCommand(new TaskShowByIndexCommand());
        registryCommand(new TaskStartByIdCommand());
        registryCommand(new TaskStartByIndexCommand());
        registryCommand(new TaskUpdateByIdCommand());
        registryCommand(new TaskUpdateByIndexCommand());

        registryCommand(new UserRegistryCommand());
        registryCommand(new UserChangePasswordCommand());
        registryCommand(new UserLoginCommand());
        registryCommand(new UserLogoutCommand());
        registryCommand(new UserViewProfileCommand());
        registryCommand(new UserUpdateProfileCommand());
        registryCommand(new UserRemoveCommand());
        registryCommand(new UserLockCommand());
        registryCommand(new UserUnlockCommand());
    }

    private void registryCommand(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        final User user = userService.create("user", "user", "user@m.ru");
        final User test = userService.create("test", "test", "test@m.ru");
        final User admin = userService.create("admin", "admin", "admin@m.ru", Role.ADMIN);

        projectService.create(user.getId(), "P1", "D1");
        projectService.create(user.getId(), "P2", "D2");
        projectService.create(user.getId(), "P3", "D3");
        projectService.create(admin.getId(), "P4", "D4");

        taskService.create(user.getId(), "T1", "T1");
        taskService.create(user.getId(), "T2", "T2");
        taskService.create(user.getId(), "T3", "T3");
        taskService.create(user.getId(), "T4", "T4");
        taskService.create(admin.getId(), "T5", "T5");
        taskService.create(admin.getId(), "T6", "T6");
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String command) {
        if (StringUtils.isBlank(command)) return;
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(final String argument) {
        if (StringUtils.isBlank(argument)) return;
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    public void run(final String... args) {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        processCommands();
    }

}