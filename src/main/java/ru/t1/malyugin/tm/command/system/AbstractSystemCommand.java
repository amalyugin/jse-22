package ru.t1.malyugin.tm.command.system;

import org.apache.commons.lang3.ArrayUtils;
import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}