package ru.t1.malyugin.tm.command.project;

import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-change-status-by-index";

    private static final String DESCRIPTION = "Change project status by index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.print("ENTER PROJECT INDEX: ");
        final Integer projectIndex = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        final Status status = Status.getStatusByIndex(statusIndex);
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, projectIndex, status);
    }

}