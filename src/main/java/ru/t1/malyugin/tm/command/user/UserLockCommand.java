package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    private static final String NAME = "user-lock";

    private static final String DESCRIPTION = "user lock";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

}