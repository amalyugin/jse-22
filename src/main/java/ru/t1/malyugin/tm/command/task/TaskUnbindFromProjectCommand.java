package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private static final String NAME = "task-unbind-from-project";

    private static final String DESCRIPTION = "Unbind Task from Project";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

}