package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private static final String NAME = "user-registry";

    private static final String DESCRIPTION = "registry new user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        System.out.println(user);
    }

}