package ru.t1.malyugin.tm.command.project;

import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-show-by-index";

    private static final String DESCRIPTION = "Show project by index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        renderProject(project);
    }

}