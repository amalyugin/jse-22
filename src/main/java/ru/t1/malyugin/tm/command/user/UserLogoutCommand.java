package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "user-logout";

    private static final String DESCRIPTION = "user logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}