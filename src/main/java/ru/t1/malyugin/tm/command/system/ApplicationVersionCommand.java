package ru.t1.malyugin.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private static final String NAME = "version";

    private static final String DESCRIPTION = "Show version info";

    private static final String ARGUMENT = "-v";

    private static final String VERSION = "1.22.0";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.printf("[VERSION]\n%s\n", VERSION);
    }

}