package ru.t1.malyugin.tm.command;

import ru.t1.malyugin.tm.api.model.ICommand;
import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getArgument();

    public abstract String getName();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        boolean isName = (name != null && !name.trim().isEmpty());
        boolean isArgument = (argument != null && !argument.trim().isEmpty());
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += (isName ? name + (isArgument ? ", " : "") : "");
        result += (isArgument ? argument : "");
        result += (isDescription ? " -> " + description : "");

        return result;
    }

}