package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        final String argument = command.getArgument();
        if (!StringUtils.isBlank(name)) mapByName.put(name.trim(), command);
        if (!StringUtils.isBlank(argument)) mapByArgument.put(argument.trim(), command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return mapByArgument.get(argument.trim());
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return mapByName.get(name.trim());
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return mapByName.values();
    }

}