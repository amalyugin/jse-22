package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IRepository;
import ru.t1.malyugin.tm.api.service.IService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        M model = repository.removeById(id.trim());
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        M model = repository.removeByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public void removeAll(final List<M> models) {
        repository.removeAll(models);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return repository.findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M findOneById(final String id) {
        if (StringUtils.isBlank(id)) return null;
        return repository.findOneById(id.trim());
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<M> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

}