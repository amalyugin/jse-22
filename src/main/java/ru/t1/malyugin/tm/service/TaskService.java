package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        if (StringUtils.isBlank(description)) throw new DescriptionEmptyException();
        return repository.create(userId.trim(), name.trim(), description.trim());
    }

    @Override
    public Task create(final String userId, final String name) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        return repository.create(userId.trim(), name.trim());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        return repository.findAllByProjectId(userId.trim(), projectId.trim());
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = repository.findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = repository.findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final Task task = repository.findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        final Task task = repository.findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}