package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(final IUserRepository userRepository,
                       final IProjectRepository projectRepository,
                       final ITaskRepository taskRepository) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isBlank(login)) return null;
        return repository.findOneByLogin(login.trim());
    }

    @Override
    public User findOneByEmail(final String email) {
        if (StringUtils.isBlank(email)) return null;
        return repository.findOneByEmail(email.trim());
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (StringUtils.isBlank(login)) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (StringUtils.isBlank(email)) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        final User user = repository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        final User user = repository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        final User user = repository.create(login.trim(), HashUtil.salt(password.trim()));
        user.setRole(Role.USUAL);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        final User user = repository.create(login.trim(), HashUtil.salt(password.trim()));
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        final User user = findOneByLogin(login.trim());
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        final User user = findOneByEmail(email.trim());
        return remove(user);
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        final User removedUser = super.remove(user);
        if (removedUser == null) throw new UserNotFoundException();
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        return user;
    }

}